/**
 * 
 */
package tdd.examples.fizzbuzz;

import static junit.framework.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static tdd.examples.fizzbuzz.FizzBuzz.FIZZ;
import static tdd.examples.fizzbuzz.FizzBuzz.BUZZ;
import static tdd.examples.fizzbuzz.FizzBuzz.FIZZBUZZ;

/**
 * Test cases for {@link FizzBuzz}
 * 
 * @author Krishna Lingamallu
 * 
 */
public class FizzBuzzTest {

    /* Instance under test */
    private FizzBuzz fizzBuzz;

    @Before
    public void setUp() {
	fizzBuzz = new FizzBuzz();
    }

    @Test
    public void numberThatDoesNotMatchFizzBuzz() {
	assertEquals("1", fizzBuzz.translate(1));
	assertEquals("2", fizzBuzz.translate(2));
	assertEquals("4", fizzBuzz.translate(4));
	assertEquals("7", fizzBuzz.translate(7));

    }

    @Test
    public void fizzIsReturnedForNumberThatIsMultipleOf3Only() {
	assertEquals(FIZZ, fizzBuzz.translate(3));
	assertEquals(FIZZ, fizzBuzz.translate(6));
	assertEquals(FIZZ, fizzBuzz.translate(9));
	assertEquals(FIZZ, fizzBuzz.translate(12));
	assertEquals(FIZZ, fizzBuzz.translate(18));

    }

    @Test
    public void buzzIsReturnedForNumberThatIsMultipleOf5Only() {
	assertEquals(BUZZ, fizzBuzz.translate(5));
	assertEquals(BUZZ, fizzBuzz.translate(10));
	assertEquals(BUZZ, fizzBuzz.translate(20));
	assertEquals(BUZZ, fizzBuzz.translate(25));
    }

    @Test
    public void fizzbuzzIsReturnedForNumberThatIsMultipleOf15() {
	assertEquals(FIZZBUZZ, fizzBuzz.translate(15));
	assertEquals(FIZZBUZZ, fizzBuzz.translate(30));
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionThrownWhenStartNumberIsGreaterThanEndNumber() {
	fizzBuzz.translate(10, 9);
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionThrownWhenStartNumberIsLessThan1() {
	fizzBuzz.translate(0, 9);
    }

    @Test
    public void rangeFrom1to20WithAllCombinations() {
	List<String> expected = new ArrayList<>(20);
	expected.add("1");
	expected.add("2");
	expected.add(FIZZ);
	expected.add("4");
	expected.add(BUZZ);
	expected.add(FIZZ);
	expected.add("7");
	expected.add("8");
	expected.add(FIZZ);
	expected.add(BUZZ);
	expected.add("11");
	expected.add(FIZZ);
	expected.add("13");
	expected.add("14");
	expected.add(FIZZBUZZ);
	expected.add("16");
	expected.add("17");
	expected.add(FIZZ);
	expected.add("19");
	expected.add(BUZZ);
	
	List<String> translated = fizzBuzz.translate(1, 20);
	
	assertEquals(expected, translated);
	fizzBuzz.process(1, 20);
    }

    @Test
    public void rangeFrom1To2WithOutAnyFizzBuzzMatches() {
	List<String> expected = new ArrayList<>(6);
	expected.add("1");
	expected.add("2");
	
	List<String> translated = fizzBuzz.translate(1, 2);
	
	assertEquals(expected, translated);
    }
    
    @Test
    public void rangeFrom5to14WithoutMultipleOf15() {
	List<String> expected = new ArrayList<>(10);
	expected.add(BUZZ);
	expected.add(FIZZ);
	expected.add("7");
	expected.add("8");
	expected.add(FIZZ);
	expected.add(BUZZ);
	expected.add("11");
	expected.add(FIZZ);
	expected.add("13");
	expected.add("14");
	
	List<String> translated = fizzBuzz.translate(5, 14);
	
	assertEquals(expected, translated);
    }

    @Test
    public void rangeFrom5To6WithOnlyFizzOrBuzzMatches() {
	List<String> expected = new ArrayList<>(6);
	expected.add(BUZZ);
	expected.add(FIZZ);
	
	List<String> translated = fizzBuzz.translate(5, 6);
	
	assertEquals(expected, translated);
    }
}