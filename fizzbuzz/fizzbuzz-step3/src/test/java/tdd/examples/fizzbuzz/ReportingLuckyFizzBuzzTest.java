/**
 * 
 */
package tdd.examples.fizzbuzz;

import static junit.framework.Assert.assertEquals;
import static tdd.examples.fizzbuzz.FizzBuzz.SEPARATOR;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for {@link ReportingLuckyFizzBuzz}
 * @author Krishna Lingamallu
 * 
 */
public class ReportingLuckyFizzBuzzTest extends LuckyFizzBuzzTest {

    @Before
    public void setUp() {
	fizzBuzz = new ReportingLuckyFizzBuzz();
    }
    
    /**
     * Test method for
     * {@link tdd.examples.fizzbuzz.ReportingLuckyFizzBuzz#convertToResult(java.util.List, java.lang.String)}
     * .
     */
    @Test
    public void testConvertToResult() {
	List<String> expected = rangeFrom1to20ExpectedTranslatedNumbers();
	List<String> translated = fizzBuzz.translate(1, 20);

	assertEquals(expected, translated);
	FizzBuzzResult result = ((ReportingLuckyFizzBuzz)fizzBuzz).convertToResult(translated, SEPARATOR);
	
	assertEquals(4, result.getFizzCounter());
	assertEquals(3, result.getBuzzCounter());
	assertEquals(1, result.getFizzBuzzCounter());
	assertEquals(2, result.getLuckyCounter());
	assertEquals(10, result.getNumberCounter());

	assertEquals(expected, result.getTranslatedSequence());
	assertEquals("1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz", result.getFizzBuzzString());
    }
}
