/**
 * 
 */
package tdd.examples.fizzbuzz;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static tdd.examples.fizzbuzz.FizzBuzz.BUZZ;
import static tdd.examples.fizzbuzz.FizzBuzz.FIZZ;
import static tdd.examples.fizzbuzz.FizzBuzz.FIZZBUZZ;
import static tdd.examples.fizzbuzz.LuckyFizzBuzz.LUCKY;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * Test cases for {@link LuckyFizzBuzz}
 * 
 * @author Krishna Lingamallu
 * 
 */
public class LuckyFizzBuzzTest extends FizzBuzzTest {

    @Before
    public void setUp() {
	fizzBuzz = new LuckyFizzBuzz();
    }

    /* Tests from super class that are not valid due to change in rules. They are replaced by 3 tests immediately below*/
    @Override
    public void fizzIsReturnedForNumberThatIsMultipleOf3Only() {
    }

    @Override
    public void fizzbuzzIsReturnedForNumberThatIsMultipleOf15() {
    }
    
    @Override
    public void buzzIsReturnedForNumberThatIsMultipleOf5Only() {
    }
    

    @Test
    public void fizzIsReturnedForNumberThatIsMultipleOf3OnlyAndDoesNotContain3() {
	assertFalse(FIZZ.equals(fizzBuzz.translate(3)));
	assertEquals(FIZZ, fizzBuzz.translate(6));
	assertEquals(FIZZ, fizzBuzz.translate(9));
	assertEquals(FIZZ, fizzBuzz.translate(12));
	assertEquals(FIZZ, fizzBuzz.translate(18));
	assertEquals(FIZZ, fizzBuzz.translate(21));
    }

    @Test
    public void buzzIsReturnedForNumberThatIsMultipleOf5OnlyAndDoesNotContain3() {
	assertEquals(BUZZ, fizzBuzz.translate(5));
	assertEquals(BUZZ, fizzBuzz.translate(10));
	assertEquals(BUZZ, fizzBuzz.translate(20));
	assertEquals(BUZZ, fizzBuzz.translate(25));
	assertFalse(BUZZ.equals(fizzBuzz.translate(35)));
    }


    @Test
    public void fizzbuzzIsReturnedForNumberThatIsMultipleOf15AndDoesNotContain3() {
	assertEquals(FIZZBUZZ, fizzBuzz.translate(15));
	assertFalse(FIZZBUZZ.equals(fizzBuzz.translate(30)));
	assertEquals(FIZZBUZZ, fizzBuzz.translate(45));
    }

    @Test
    public void luckyIsReturnedForNumberThatContains3() {
	assertEquals(LUCKY, fizzBuzz.translate(3));
	assertEquals(LUCKY, fizzBuzz.translate(13));
	assertEquals(LUCKY, fizzBuzz.translate(23));
	assertEquals(LUCKY, fizzBuzz.translate(35));
    }

    @Test
    public void rangeFrom1to20WithAllCombinations() {
	List<String> expected = rangeFrom1to20ExpectedTranslatedNumbers();
	List<String> translated = fizzBuzz.translate(1, 20);

	assertEquals(expected, translated);
	fizzBuzz.process(1, 20);
    }
    
    protected List<String> rangeFrom1to20ExpectedTranslatedNumbers(){
	List<String> expected = new ArrayList<>(20);
	expected.add("1");
	expected.add("2");
	expected.add(LUCKY);
	expected.add("4");
	expected.add(BUZZ);
	expected.add(FIZZ);
	expected.add("7");
	expected.add("8");
	expected.add(FIZZ);
	expected.add(BUZZ);
	expected.add("11");
	expected.add(FIZZ);
	expected.add(LUCKY);
	expected.add("14");
	expected.add(FIZZBUZZ);
	expected.add("16");
	expected.add("17");
	expected.add(FIZZ);
	expected.add("19");
	expected.add(BUZZ);
	return expected;
    }

    @Test
    public void rangeFrom5to14WithoutMultipleOf15() {
	List<String> expected = new ArrayList<>(10);
	expected.add(BUZZ);
	expected.add(FIZZ);
	expected.add("7");
	expected.add("8");
	expected.add(FIZZ);
	expected.add(BUZZ);
	expected.add("11");
	expected.add(FIZZ);
	expected.add(LUCKY);
	expected.add("14");

	List<String> translated = fizzBuzz.translate(5, 14);

	assertEquals(expected, translated);
    }

    @Test
    public void rangeFrom12To13WithOnlyLuckyOrFizzMatches() {
	List<String> expected = new ArrayList<>(2);
	expected.add(FIZZ);
	expected.add(LUCKY);

	List<String> translated = fizzBuzz.translate(12, 13);

	assertEquals(expected, translated);
    }

    @Test
    public void rangeFrom39To40WithOnlyLuckyOrBuzzMatches() {
	List<String> expected = new ArrayList<>(2);
	expected.add(LUCKY);
	expected.add(BUZZ);

	List<String> translated = fizzBuzz.translate(39, 40);

	assertEquals(expected, translated);
    }

    @Test
    public void rangeFrom30To39WithOnlyLuckyMatches() {
	List<String> expected = new ArrayList<>(10);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);
	expected.add(LUCKY);

	List<String> translated = fizzBuzz.translate(30, 39);

	assertEquals(expected, translated);
	fizzBuzz.process(30, 39);
    }

    @Test
    public void rangeFrom4to12WithoutLuckyMatches() {
	List<String> expected = new ArrayList<>(9);
	expected.add("4");
	expected.add(BUZZ);
	expected.add(FIZZ);
	expected.add("7");
	expected.add("8");
	expected.add(FIZZ);
	expected.add(BUZZ);
	expected.add("11");
	expected.add(FIZZ);

	List<String> translated = fizzBuzz.translate(4, 12);

	assertEquals(expected, translated);
    }
}