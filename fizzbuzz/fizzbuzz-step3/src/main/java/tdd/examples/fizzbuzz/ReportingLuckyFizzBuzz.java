/**
 * 
 */
package tdd.examples.fizzbuzz;

import java.util.List;

/**
 * Specialisation of {@link LuckyFizzBuzz} that generates report containing the
 * counter for each type of number translation.
 * 
 * @author Krishna Lingamallu
 * 
 */
public class ReportingLuckyFizzBuzz extends LuckyFizzBuzz {

    private static final String LABEL_VALUE_SEPARATOR = ": ";
    private static String LINE_SEPARATOR = System.getProperty("line.separator");

    /**
     *  
     * {@inheritDoc}
     */
    @Override
    protected void print(List<String> fizzBuzzed, String separator) {
	FizzBuzzResult result = convertToResult(fizzBuzzed, separator);
	printReport(result);
    }

    protected FizzBuzzResult convertToResult(List<String> translated, String separator) {
	boolean firstElement = true;
	int fizzCounter = 0;
	int buzzCounter = 0;
	int fizzbuzzCounter = 0;
	int luckyCounter = 0;
	int numberCounter = 0;

	StringBuilder fizzbuzzSB = new StringBuilder();
	for (String val : translated) {
	    if (firstElement) {
		firstElement = false;
	    } else {
		fizzbuzzSB.append(SEPARATOR);
	    }

	    fizzbuzzSB.append(val);

	    switch (val) {
	    case FIZZ:
		fizzCounter++;
		break;
	    case BUZZ:
		buzzCounter++;
		break;
	    case FIZZBUZZ:
		fizzbuzzCounter++;
		break;
	    case LUCKY:
		luckyCounter++;
		break;
	    default:
		numberCounter++;
	    }
	}

	FizzBuzzResult result = new FizzBuzzResult(translated, fizzbuzzSB.toString(), fizzCounter, buzzCounter,
			fizzbuzzCounter, luckyCounter, numberCounter);

	return result;
    }

    private void printReport(FizzBuzzResult result) {
	StringBuilder builder = new StringBuilder();

	builder.append(result.getFizzBuzzString());
	builder.append(LINE_SEPARATOR);

	builder.append(FIZZ).append(LABEL_VALUE_SEPARATOR).append(result.getFizzCounter());
	builder.append(LINE_SEPARATOR);

	builder.append(BUZZ).append(LABEL_VALUE_SEPARATOR).append(result.getBuzzCounter());
	builder.append(LINE_SEPARATOR);

	builder.append(FIZZBUZZ).append(LABEL_VALUE_SEPARATOR).append(result.getFizzBuzzCounter());
	builder.append(LINE_SEPARATOR);

	builder.append(LUCKY).append(LABEL_VALUE_SEPARATOR).append(result.getLuckyCounter());
	builder.append(LINE_SEPARATOR);

	builder.append("integer").append(LABEL_VALUE_SEPARATOR).append(result.getNumberCounter());
	System.out.println(builder.toString());
    }
}