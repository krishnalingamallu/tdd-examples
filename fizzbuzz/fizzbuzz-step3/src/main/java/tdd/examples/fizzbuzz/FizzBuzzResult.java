/**
 * 
 */
package tdd.examples.fizzbuzz;

import java.util.List;

/**
 * Container to store FizzBuzz translation results.
 * 
 * @author Krishna Lingamallu
 * 
 */
public class FizzBuzzResult {
    private List<String> translatedSequence;
    private String fizzBuzzString;
    private int fizzCounter;
    private int buzzCounter;
    private int fizzBuzzCounter;
    private int luckyCounter;
    private int numberCounter;

    /**
     * Constructor.
     * 
     * @param translatedSequence
     *            Translated Fizz buzz numbers
     * @param fizzBuzzString
     *            Numbers as translated string
     * @param fizzCounter
     *            Count of numbers translated to fizz
     * @param buzzCounter
     *            Count of numbers translated to buzz
     * @param fizzBuzzCounter
     *            Count of numbers translated to fizzbuzz
     * @param luckyCounter
     *            Count of numbers translated to lucky
     * @param numberCounter
     *            Count of numbers translated to number itself
     */
    public FizzBuzzResult(final List<String> translatedSequence, final String fizzBuzzString, final int fizzCounter,
		    final int buzzCounter, final int fizzBuzzCounter, final int luckyCounter, final int numberCounter) {
	this.translatedSequence = translatedSequence;
	this.fizzBuzzString = fizzBuzzString;
	this.fizzCounter = fizzCounter;
	this.buzzCounter = buzzCounter;
	this.fizzBuzzCounter = fizzBuzzCounter;
	this.luckyCounter = luckyCounter;
	this.numberCounter = numberCounter;
    }

    /**
     * @return the translatedSequence
     */
    public List<String> getTranslatedSequence() {
	return translatedSequence;
    }

    /**
     * @return the fizzBuzzString
     */
    public String getFizzBuzzString() {
	return fizzBuzzString;
    }

    /**
     * @return the fizzCounter
     */
    public int getFizzCounter() {
	return fizzCounter;
    }

    /**
     * @return the buzzCounter
     */
    public int getBuzzCounter() {
	return buzzCounter;
    }

    /**
     * @return the fizzBuzzCounter
     */
    public int getFizzBuzzCounter() {
	return fizzBuzzCounter;
    }

    /**
     * @return the luckyCounter
     */
    public int getLuckyCounter() {
	return luckyCounter;
    }

    /**
     * @return the numberCounter
     */
    public int getNumberCounter() {
	return numberCounter;
    }
}
