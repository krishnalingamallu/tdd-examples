/**
 * 
 */
package tdd.examples.fizzbuzz;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is responsible for translating a contiguous range of numbers into
 * a fizz buzz sequence as per the rules specified below and printing the sequence.
 * <ul>
 * <li>the number</li>
 * <li>'fizz' for numbers that are multiples of 3</li>
 * <li>'buzz' for numbers that are multiples of 5</li>
 * <li>'fizzbuzz' for numbers that are multiples of 15</li>
 * </ul>
 * 
 * @author Krishna Lingamallu
 * 
 */
public class FizzBuzz {

    protected static final String FIZZ = "fizz";
    protected static final String BUZZ = "buzz";
    protected static final String FIZZBUZZ = FIZZ + BUZZ;
    protected static final String SEPARATOR = " ";

    /**
     * Print the fizz buzz sequence for a contiguous range of numbers to the console.
     * 
     * @param start - Starting number
     * @param end   - Ending number
     */
    public void process(int start, int end) {
	List<String> fizzBuzzed = translate(start, end);
	print(fizzBuzzed, SEPARATOR);
    }

    protected List<String> translate(int start, int end) {
	validateRange(start, end);
	int num = end - start + 1;
	List<String> fizzBuzzed = new ArrayList<>(num);

	for (int val = start; val <= end; val++) {
	    fizzBuzzed.add(translate(val));
	}
	return fizzBuzzed;
    }

    protected String translate(int value) {
	validateNumberIsPositive(value);
	boolean multipleOf3 = (value % 3 == 0);
	boolean multipleOf5 = (value % 5 == 0);

	String result;
	if (multipleOf3 && multipleOf5) {
	    result = FIZZBUZZ;
	} else if (multipleOf3) {
	    result = FIZZ;
	} else if (multipleOf5) {
	    result = BUZZ;
	} else {
	    result = String.valueOf(value);
	}
	return result;
    }

    private void validateRange(int start, int end) {
	validateNumberIsPositive(start);
	if (start > end) {
	    throw new IllegalArgumentException("start number must not be greater than the end number");
	}
    }

    protected void validateNumberIsPositive(int value) {
	if (value < 1) {
	    throw new IllegalArgumentException("start number must be positive number");
	}
    }

    protected void print(List<String> fizzBuzzed, String separator) {
	boolean firstElement = true;

	StringBuilder result = new StringBuilder();
	for (String val : fizzBuzzed) {
	    if (firstElement) {
		firstElement = false;
	    } else {
		result.append(separator);
	    }

	    result.append(val);
	}
	System.out.println(result);
    }
}