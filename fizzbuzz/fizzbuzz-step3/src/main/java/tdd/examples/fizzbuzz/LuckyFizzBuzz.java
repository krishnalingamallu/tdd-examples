/**
 * 
 */
package tdd.examples.fizzbuzz;

/**
 * Specialisation of {@link FizzBuzz} that translates a number to 'lucky' if the number contains 3.
 * @author Krishna Lingamallu
 * 
 */
public class LuckyFizzBuzz extends FizzBuzz {
    protected static final String LUCKY = "lucky";
    protected static final String THREE = "3";

    /**
     * Returns 'lucky' if the number contains 3.
     * {@inheritDoc}
     */
    @Override
    protected String translate(int value) {
	super.validateNumberIsPositive(value);
	if (String.valueOf(value).contains(THREE)) {
	    return LUCKY;
	}
	return super.translate(value);
    }
}
